import React, { Component } from 'react';
import axios from 'axios';
class About extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listData: []
        }
    }
    componentDidMount() {
        this.getData();
    }
    getData = async () => {
        try {
            let obj = JSON.parse(localStorage.getItem("details")).sessionDetails;
            let headers = {
                'Access-Control-Allow-Origin': '*',
                'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem("details")).accessToken
            }
            const response = await axios.get('/posts?expiration=' + obj.cookie.expires, { headers: headers });
            if (response.data.length > 0) {
                this.setState({ listData: response.data });
            }
            alert(response.data);
        }
        catch (error) {
            alert(error);
            console.log(error);
        }
    }
    render() {
        return (
            <div>
                <ul>
                    {this.state.listData && this.state.listData.map((d, index) => {
                        return (
                            <li key={index}>{d.username}:{d.title}</li>
                        )
                    })}
                </ul>
            </div>
        );
    }
}

export default About;