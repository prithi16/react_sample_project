import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux';
import { incrementSaga, decrementSaga } from './Container/Actions/sample.actions';
import axios from 'axios';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import About from './Components/About';
import Users from './Components/Users';

class App extends Component {
  constructor() {
    super();
    this.state = {
      response: null,
      username: "",
      password: '',
      show: true,
      selectedFile: null,
      filePath: "",
      result: ""
    }
  }
  myChangeHandler = (event) => {
    var name = event.target.name;
    this.setState({
      [name]: event.target.value
    });
  }
  componentDidMount() {

  }
  datacancel = () => {
    this.setState({
      response: null, show: true
    })
  }
  dataSessionExpire = async () => {
    try {
      const response = await axios.get('api/sessionExpireCheck?expiration=' + localStorage.getItem("Expiraton"));
      alert(response.data);
      if (response.data === `session expired`)
        this.datacancel();
    } catch (error) {
      alert(error);
      console.log(error);
    }

    // this.callApi()
    //   .then(res => this.setState({
    //     response: res.data, show: false
    //   }), this)
    //   .catch(err => console.log(err));
  }
  dataSubmit = async () => {
    // try {
    //   const headers = {
    //     'Content-Type': 'application/json',
    //     'Accept': 'text/plain'
    //   }
    //   const response = await axios.get('/api/hello?username=' + this.state.username, { headers: headers });
    //   console.log(response);
    //   const data = response.data.Message;
    //   if (response.data.session)
    //     localStorage.setItem("Expiraton", response.data.session.cookie.expires);
    //   this.setState({ response: data, show: false });
    // } catch (error) {
    //   console.log(error);
    // }

    // this.callApi()
    //   .then(res => this.setState({
    //     response: res.data, show: false
    //   }), this)
    //   .catch(err => console.log(err));
    this.makePostRequest();
  }
  makePostRequest = async () => {
    let params =
    {
      "username": "Test"
    }
    let headers = {
      'Access-Control-Allow-Origin': '*'
    }
    let res = await axios.post('/login', params, { headers: headers });
    console.log(res.data);
    this.setState({ result: res.data });
    localStorage.setItem('details', JSON.stringify(res.data));
  }
  // callApi = async () => {
  //   const response = await fetch('/api/hello?username=' + this.state.username);
  //   const body = await response.json();
  //   // if (response.status !== 200) throw Error(body.message);
  //   return body;
  // };
  onChangeHandler = event => {
    this.setState({
      selectedFile: event.target.files,
      loaded: 0,
    })
  }
  onClickHandler = async () => {
    const data = new FormData();
    for (var x = 0; x < this.state.selectedFile.length; x++) {
      data.append('mypic', this.state.selectedFile[x])
    }
    // data.append('mypic', this.state.selectedFile);
    let headers = {
      'Access-Control-Allow-Origin': '*'
    }
    let res = await axios.post('/api/Upload', data, { headers: headers });
    console.log(res);
    alert(res.data);
  }
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/about">About</Link>
                </li>
                <li>
                  <Link to="/users">Users</Link>
                </li>
              </ul>
            </nav>
            <Switch>
              <Route path="/about">
                <About />
              </Route>
              <Route path="/users">
                <Users />
              </Route>
            </Switch>
          </div>
        </Router>
        {/* <div>Increment:{this.props.Age}</div>
        <div>Decrement:{this.props.AgeB}</div> */}
        {this.state.show ? <form>
          <p>Username: <input type='text' name="username" onChange={this.myChangeHandler} /> </p>
          <p>Password: <input type='password' name="password" onChange={this.myChangeHandler} /></p>
        </form> : null}
        {this.state.response ? <div>ApiData:{this.state.response}</div> : ''}
        {/* <button onClick={() => this.props.increment(this.props.AgeB)}>increment</button>
        <button onClick={() => this.props.decrement(this.props.Age)}>decrement</button> */}
        {/* <input type="file" name="mypic" onChange={this.onChangeHandler} multiple />

        <button type="button" className="btn btn-success btn-block" onClick={this.onClickHandler}>Upload</button> */}
        <br />
        <button onClick={this.dataSubmit}>submit</button>
        <button onClick={this.datacancel}>cancel</button>
        {/* <button onClick={this.dataSessionExpire}>SessionExpireCheck</button> */}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    Age: state.RA.Age,
    AgeB: state.RB.AgeB
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    increment: (b) => dispatch(incrementSaga(b)),
    decrement: (a) => dispatch(decrementSaga(a))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
