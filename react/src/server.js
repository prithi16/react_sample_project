const express = require('express');
const bodyParser = require('body-parser');
var multer = require('multer');
const path = require("path")
const axios = require('axios');
var cookieParser = require('cookie-parser');
var session = require('express-session');
const { v4: uuidv4 } = require('uuid');

const app = express();
const port = process.env.PORT || 5000;
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const url = "http://localhost:3002/loginCheck";
// View Engine Setup 
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")

 // var upload = multer({ dest: "" }) 
// If you do not want to use diskStorage then uncomment it 
var filePath="uploads";
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    
    // Uploads is the Upload_folder_name 
    cb(null,filePath)
  },
  filename: function (req, file, cb) {
    console.log(file);
    cb(null, file.originalname)
  }
})

// Define the maximum size for uploading 
// picture i.e. 1 MB. it is optional 
const maxSize = 1 * 1000 * 1000;

var upload = multer({
  storage: storage,
  limits: { fileSize: maxSize },
  fileFilter: function (req, file, cb) {
    return cb(null, true);
  }
}).any();
app.use(session({
  secret: "shh its a screte",
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true, maxAge: 300000 }
}));
app.use(function (req, res, next) {
  req.sessionId = () => uuidv4();
  console.log(req.session);
  console.log("test1");
  next();
})
app.get('/api/hello', async (req, res, next) => {
  try {
    console.log(req.headers);
    const urlFetch = url + '?username=' + req.query.username;
    const data1 = await getData(urlFetch);
    console.log(data1);
    if (data1 === "login success") {
      req.session.cookie.maxAge = 300000;
      console.log(req.session);

    }
    else {
      console.log(req.session);
      req.session = null;
      console.log(req.session);
    }
    var result = {};
    result.session = req.session;
    result.Message = data1;
    res.json(result);
    //res.send(data1);
    // console.log(req.session);
    // console.log(req.session.cookie._expires);
    // if(new Date() < new Date(req.session.cookie._expires)){
    // const urlFetch =url + '?username=' + req.query.username;
    // const data1= await getData(urlFetch);
    // // req.session.cookie.maxAge=300000;
    // res.send(data1);
    // }
    // else {
    //   throw new Error('session expired')
    // }
    // axios.get(url + '?username=' + req.query.username).then(resp => {

    //   if (resp.data.length === 0) {
    //     axios.get('http://localhost:3002/error').then(ges => {
    //       console.log(req.sessionId);
    //       res.send(ges.data);
    //     });
    //   }
    //   else {

    //     axios.get('http://localhost:3002/login').then(b => {
    //       console.log(b);
    //       res.send(b.data);
    //     });
    //     // res.json(resp.data);
    //   }
    // });

  } catch (error) {
    console.log(error);
    return error;
  }
});
const successData = async url => {
  try {
    const response = await axios.get('http://localhost:3002/login');
    const data1 = response.data;
    console.log(data1.data);
    return data1.data;
  } catch (error) {
    console.log(error);

  }
}
const errorData = async url => {
  try {
    const response = await axios.get('http://localhost:3002/error');
    const data2 = response.data;
    console.log(data2.data);
    return data2.data;
  } catch (error) {
    console.log(error);

  }
}
const getData = async url => {
  try {
    const response = await axios.get(url);
    const data = response.data;
    console.log(data);
    if (data.length === 0) {
      return errorData(url);
    }
    else {
      const suss = successData(url);
      return suss;
    }
  } catch (error) {
    console.log(error);

  }
}
app.get('/api/sessionExpireCheck', (req, res) => {
  console.log(req.query.expiration);
  if (req.session.cookie._expires && new Date() < new Date(req.query.expiration)) {
    req.session.cookie.maxAge = 300000;
    res.send(`session avilable`);
  }
  else {
    res.send(`session expired`);
  }

});
app.post("/api/Upload", function (req, res) {
  console.log("upload");
  upload(req, res, function (err) {
    if (err) {
      console.log(err);
      return res.end("Something went wrong!");
    }
    return res.end("File uploaded sucessfully!.");
  });
});
app.listen(port, () => console.log(`Listening on port ${port}`));