import { sampleAlerts } from './constants/sample.constants';
const initialState = {
    Age: 1
}
const reducer = (state = initialState, Action) => {
    if (Action.type === sampleAlerts.INCREMENT) {
        return {
            ...state,
            Age: state.Age + Action.val
        }
    }
    // if (Action.type === sampleAlerts.DECREMENT) {
    //     return {
    //         ...state,
    //         Age: state.Age - 1
    //     }
    // }
    return state;

}
export default reducer;