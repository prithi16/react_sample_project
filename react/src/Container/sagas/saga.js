import { takeLatest, takeEvery, put, call, fork, all } from 'redux-saga/effects';
import { sampleAlerts } from '../constants/sample.constants';

function* FeachUser(action) {
    try {
        var user = 25;
        yield put({ type: sampleAlerts.INCREMENT, val: user });
    }
    catch (e) {

    }
}
function* DecrementUser(action) {
    try {
        var user = 1;
        yield put({ type: sampleAlerts.DECREMENT, val: user });
    }
    catch (e) {

    }
}

function* WatchAge() {
    yield takeEvery(sampleAlerts.INCREMENT_SAGA, FeachUser);
    // yield takeEvery("DECREMENT_SAGA",DecrementUser);
}
function* saga2() {
    yield takeEvery(sampleAlerts.DECREMENT_SAGA, DecrementUser);
}
function* rootSaga() {
    yield all([
        fork(saga2),
        fork(WatchAge)
        // yield takeEvery("INCREMENT_SAGA",FeachUser),
        // yield takeEvery("DECREMENT_SAGA",DecrementUser)
    ]);
}
export default rootSaga;