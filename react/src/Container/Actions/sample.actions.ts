import { sampleAlerts} from '../constants/sample.constants';

// export const sampleActions = {
//     incrementSaga,
//     decrementSaga
// }
export function incrementSaga(obj: any) {
  return {  type: sampleAlerts.INCREMENT_SAGA,
        payload: obj
  }
}
export function decrementSaga(obj: any) {
  return {  type: sampleAlerts.DECREMENT_SAGA,
        payload: obj
  }
}