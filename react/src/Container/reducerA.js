import { sampleAlerts } from './constants/sample.constants';
const intialState = {
    AgeB: 5
}
const reducer = (state = intialState, Action) => {
    if (Action.type === sampleAlerts.DECREMENT) {
        return {
            ...state,
            AgeB: state.AgeB - Action.val
        }
    }
    return state;
}
export default reducer;